using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Kk.CsxCore
{
    public static class LangUtils
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T[] OrEmpty<T>(this T[] list) => list ?? EmptyArrayHolder<T>.EmptyArray;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IList<T> OrEmpty<T>(this IList<T> list) => list ?? EmptyArrayHolder<T>.EmptyArray;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IEnumerable<T> OrEmpty<T>(this IEnumerable<T> list) => list ?? EmptyArrayHolder<T>.EmptyArray;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T NotNull<T>(this T obj)
        {
            if (obj == null)
            {
                throw new Exception("is null");
            }

            return obj;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ClampUp(this float x, float lowerBound)
        {
            return Math.Max(x, lowerBound);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ClampDown(this float x, float upperBound)
        {
            return Math.Min(x, upperBound);
        }

        private static class EmptyArrayHolder<T>
        {
            public static readonly T[] EmptyArray = new T[0];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TV GetOrDefault<TK, TV>(this IDictionary<TK, TV> dict, TK key)
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }

            return default;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TV? GetOrNull<TK, TV>(this IDictionary<TK, TV> dict, TK key)
            where TV : struct
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string ShallowListToString<T>(this IEnumerable<T> list, string delimiter = ", ")
        {
            return list
                .Select(x => x.ToString())
                .Aggregate((a, b) => a + delimiter + b);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SharedCopyEnumerator<T> IterableCopy<T>(this IEnumerable<T> list)
        {
            return new SharedCopyEnumerator<T>(list);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int UnsignedMod(this int value, int size)
        {
            if (size == 0)
            {
                return value;
            }

            var n = value;
            while (n < 0)
            {
                n += size;
            }

            return n % size;
        }
    }
}