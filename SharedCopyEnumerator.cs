using System;
using System.Collections;
using System.Collections.Generic;

namespace Kk.CsxCore
{
    // point is - it's allocation free
    // limitation - nested loops are impossible
    // TODO: investigate if it's possible to use Dispose to signalize the end and use stack or pool to enable nested loops
    public struct SharedCopyEnumerator<T>: IEnumerator<T>, IEnumerable<T>
    {
        private static readonly List<T> SharedList = new List<T>();
        private int _index;

        public SharedCopyEnumerator(IEnumerable<T> list)
        {
            // can be abandoned if previous iteration interrupted with exception.
            // but we still report it as nested loops more dangerous than repeated exception in case of another exception

            if (SharedList.Count > 0)
            {
                throw new Exception("nested loop detected!");
            }
            
            SharedList.AddRange(list);
            _index = 0;
        }

        public void Reset()
        {
            throw new NotSupportedException("not supported");
        }

        object IEnumerator.Current => Current;

        public T Current
        {
            get
            {
                if (_index == 0)
                    throw new InvalidOperationException();

                return SharedList[_index - 1];
            }
        }

        public bool MoveNext()
        {
            _index++;
            var hasNext = SharedList.Count >= _index;
            if (!hasNext)
            {
                SharedList.Clear();
            }
            return hasNext;
        }

        public void Dispose()
        {
            Reset();
        }

        public IEnumerator<T> GetEnumerator()
        {
            _index = 0;
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}