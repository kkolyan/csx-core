using System;
using System.Collections.Generic;
using System.Linq;

namespace Kk.CsxCore.Graph
{
    /// <summary>
    /// en.wikipedia.org/wiki/Topological_sorting#Kahn.27s_algorithm
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class KahnTopologicalSorter<T> : IGraphSorter<T>
    {
        public IList<T> GetSorted(IEnumerable<T> roots, IEnumerable<Edge<T>> edges)
        {
            var result = new List<T>();
            var originalRoots = roots.ToList();
            var currentRoots = new Queue<T>(originalRoots);

            ISet<Edge<T>> edgesSet;
            if (edges is ISet<Edge<T>> set)
            {
                edgesSet = set;
            }
            else
            {
                edgesSet = new HashSet<Edge<T>>(edges);
            }

            IDictionary<T, Queue<Edge<T>>> edgesByFrom = edgesSet
                .GroupBy(edge => edge.From)
                .ToDictionary(grouping => grouping.Key, grouping => new Queue<Edge<T>>(grouping));

            IDictionary<T, List<Edge<T>>> edgesByTo = edgesSet
                .GroupBy(edge => edge.To)
                .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

            while (currentRoots.Count > 0)
            {
                var n = currentRoots.Dequeue();
                result.Add(n);

                var edgesFromN = edgesByFrom.GetOrDefault(n);
                if (edgesFromN != null)
                {
                    while (edgesFromN.Count > 0)
                    {
                        var e = edgesFromN.Dequeue();
                        var m = e.To;
                        var edgesToM = edgesByTo.GetOrDefault(m);
                        edgesToM?.Remove(e);
                        if (edgesToM == null || edgesToM.Count == 0)
                        {
                            edgesByTo.Remove(m);
                            currentRoots.Enqueue(m);
                        }
                    }

                    edgesByFrom.Remove(n);
                }
            }

            if (edgesByFrom.Count > 0 || edgesByTo.Count > 0)
            {
                throw new Exception("failed to sort topologically:" +
                                    $"\n    roots: {originalRoots.ShallowListToString()}" +
                                    $"\n    graph: {edgesSet.ShallowListToString()}" +
                                    $"\n    edgesByFrom: {edgesByFrom.SelectMany(x => x.Value).ShallowListToString()}" +
                                    $"\n    edgesByTo: {edgesByTo.SelectMany(x => x.Value).ShallowListToString()}");
            }

            return result;
        }
    }
}