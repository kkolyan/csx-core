using System.Collections.Generic;

namespace Kk.CsxCore.Graph
{
    public interface IGraphSorter<T>
    {
        IList<T> GetSorted(IEnumerable<T> roots, IEnumerable<Edge<T>> edges);
    }
}